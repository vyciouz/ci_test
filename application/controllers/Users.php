<?php
class Users extends CI_Controller{
	//Used to send argument arrays
	/*
	function _remap($method,$args){
    	if (method_exists($this, $method)){
    		$this->$method($args);
    	}else{
    		$this->index($method,$args);
    	}
    }
	*/
	
	public function view($id){
		#list.php necesita 4 argumentos
		# $dataArray, $columns, $catalogDependency, $controllerName
		# Data load
        $this->load->model('users_model');
		# Loads departments info to show it in the sidebar menu
		$data['departments'] = $this->departments_model->get_departments();
		
		print_r( $id);
		$data['dataArray'] = $this->users_model->get_user(1);
		$data['columnsToUse'] = ['USERNAME'=>'Usuario','EMAIL'=>"Correo Electrónico"];
		$data['catalogDependency'] = null;
		$data['controllerName'] = 'users';
		#/ Data load
		
		# Page information
		$data['title'] = "Usuario";
		$data['subtitle'] = "Vista General";
		$data['navnodes'] = ["Panel de Control"];
		# /Page information


		$this->load->view('templates/header',$data);
		$this->load->view('templates/navbar',$data);
		$this->load->view('templates/view',$data);
		$this->load->view('templates/footer');	
	}

	public function login(){
		# Page information
		$data['title'] = "Iniciar Sesión";
		$data['subtitle'] = "Menú principal";
		$data['navnodes'] = ["Panel de Control"];
		# /Page information
		$this->load->view('templates/header',$data);
        $this->load->view('users/login');
        $this->load->view('templates/scripts');

	}

	public function new(){
		$this->load->helper('form');
		$data['columnsToUse'] = [
            "USERNAME" => [
                'name' => "Nombre de usuario",
                'type' => 'text',
                'dependency' => null
            ],
            'EMAIL' => [
                'name' => "Correo electrónico",
                'type' => 'text',
                'dependency' => null
            ],
            'PWD' => [
                'name' => "Contraseña",
                'type' => 'password',
                'dependency' => null
            ]
        ];
		$data['catalogDependency'] =null;
		$data['formAction'] = "";
		$data['table'] = "t_users";
		# Page information
		$data['title'] = "Nuevo Usuario";
		$data['subtitle'] = "Usuarios";
		$data['navnodes'] = ['Panel de Control',"Usuarios","Nuevo Usuario"];
		# /Page information
		$data['css'] = array("select2/dist/css/select2.min.css");
		$data['js'] = ["js/new_user.js"];
		$this->load->view('templates/header',$data);
		$data['departments'] = $this->departments_model->get_departments();
		$this->load->view('templates/navbar',$data);
		$this->load->view('templates/new');
		$this->load->view('templates/scripts');
		$this->load->view('templates/footer');
	}

	public function result(){
		$data['usernanme'] = $_POST['usernanme'];
		$this->load->view('users/result',$data);
	}

	public function delete($id){
		$data['table'] = "t_users";
		$data['id'] = $id;
		$data['redirect'] = "index.php/users/list";
		$this->load->view('templates/delete',$data);
	}

	public function list(){
		#list.php necesita 4 argumentos
		# $dataArray, $columns, $catalogDependency, $controllerName
		# Data load
        $this->load->model('users_model');
		# Loads departments info to show it in the sidebar menu
		$data['departments'] = $this->departments_model->get_departments();
		
		$data['dataArray'] = $this->users_model->get_users();
		$data['columnsToUse'] = ['USERNAME'=>'Usuario','EMAIL'=>"Correo Electrónico"];
		$data['catalogDependency'] = null;
		$data['controllerName'] = 'users';
        #/ Data load
        
        # Page information
        $data['title'] = "Lista de Usuarios";
		$data['subtitle'] = "Usuarios";
		$data['navnodes'] = ["Panel de Control", "Usuarios","Lista de Usuarios"];
		//Extra CSS needed
		$data['css'] = [
		];
		//Extra JS needed
		$data['eJS'] = [
            "public/datatables.net/js/jquery.dataTables.min.js",
            "public/datatables.net-bs/js/dataTables.bootstrap.min.js"
		];
		$this->load->view('templates/header',$data);
		$this->load->view('templates/navbar',$data);
		$this->load->view('templates/list',$data);
		$this->load->view('templates/scripts',$data);
		$this->load->view('templates/footer');
	}
}
?>
