<?php
class Links extends CI_Controller{
    public function new(){
		# Data load
		$this->load->helper('form');
		# Loads departments info to show it in the sidebar menu
		$data['columnsToUse'] = [
			'TITLE'=>"Títutlo",
			'BODY'=>"Texto",
			'TYPE'=>"Tipo",
			'URL'=>"Dirección web",
			'ID_DEPT'=>"Departamento"];
		$data['catalogDependency'] = [
			'ID_DEPT'=>["TABLE"=>"cat_departments","searchInCOL"=>"ID","resultInCOL"=>"NAME"],
			'TYPE'=>["TABLE"=>"cat_link_type","searchInCOL"=>"ID","resultInCOL"=>"DESCRIPTION"]
		];
		$data['controllerName'] = 'links';

		$data['departments'] = $this->departments_model->get_departments();

		#/ Data load
		# Page information
		$data['title'] = "Nuevo Enlace";
		$data['subtitle'] = "Enlaces";
		$data['navnodes'] = ["Nombre del departamento","Editar Persona"];
		//Extra CSS needed
		$data['css'] = ["css/person.css"];
		//Extra JS needed
		// $data['js'] = [""];
		$this->load->view('templates/header',$data);
		$this->load->view('templates/navbar',$data);
        $this->load->view('templates/new',$data);
		$this->load->view('templates/scripts');
		$this->load->view('templates/footer');
	}

	public function list(){
		#list.php necesita 4 argumentos
		# $dataArray, $columns, $catalogDependency, $controllerName
		
		# Data load
		$this->load->model('links_model');
		
        $data['dataArray'] = $this->links_model->get_links();
		$data['columnsToUse'] = [
			'TITLE'=>"Títutlo",
			'BODY'=>"Texto",
			'TYPE'=>"Nombre",
			'ID_DEPT'=>"Departamento"];
		$data['catalogDependency'] = null;
		$data['controllerName'] = 'links';
		# Loads departments info to show it in the sidebar menu
		$data['departments'] = $this->departments_model->get_departments();
        #/ Data load
        
        # Page information
        $data['title'] = "Lista de Enlaces";
		$data['subtitle'] = "Empleado";
		$data['navnodes'] = ["Panel de Control", "Empleados","Lista de Empleados"];
		//Extra CSS needed
		$data['css'] = [
		];
		//Extra JS needed
		$data['eJS'] = [
            "public/datatables.net/js/jquery.dataTables.min.js",
            "public/datatables.net-bs/js/dataTables.bootstrap.min.js"
		];
		$this->load->view('templates/header',$data);
		$this->load->view('templates/navbar',$data);
		$this->load->view('templates/list',$data);
		$this->load->view('templates/scripts',$data);
		$this->load->view('templates/footer');
    }
}
?>