<?php
class Departments extends CI_Controller{
    public function index(){
		# Data load
		$data['departments'] = $this->departments_model->get_departments();
		
		# Page information
		$data['title'] = "Departamentos";
		$data['subtitle'] = "Lista";
		$data['navnodes'] = ["Departamentos"];

		$this->load->view('templates/header',$data);
		$this->load->view('templates/navbar',$data);
		$this->load->view('departments/index');
		$this->load->view('templates/scripts');
		$this->load->view('templates/footer');
	}

	public function delete($id){
		$data['table'] = "cat_departments";
		$data['id'] = $id;
		$data['redirect'] = "index.php/departments/list";
		$this->load->view('templates/delete',$data);
	}

	public function new(){
		$this->load->helper('form');
		$data['columnsToUse'] = [
			"ID" => [
                'name' => "Número de departamento.",
                'type' => 'text',
                'dependency' => null
            ],
            "NAME" => [
                'name' => "Nombre de departamento",
                'type' => 'text',
                'dependency' => null
            ],
            'TEXT_BODY' => [
                'name' => "Descripción",
                'type' => 'text',
                'dependency' => null
            ]
        ];
		$data['catalogDependency'] = null;
		$data['table'] = 'cat_departments';
		# Page information
		$data['title'] = "Nuevo Departamento";
		$data['subtitle'] = "Nuevo Departamento";
		$data['navnodes'] = ['Panel de Control',"Departamentos","Nuevo departamento"];
		# /Page information
		$data['css'] = array("bower_components/select2/dist/css/select2.min.css");
		$this->load->view('templates/header',$data);
		$data['departments'] = $this->departments_model->get_departments();
		$this->load->view('templates/navbar',$data);
		$this->load->view('templates/new');
		$this->load->view('templates/scripts');
		$this->load->view('templates/footer');
	}

	public function list(){
		#list.php necesita 4 argumentos
		# $dataArray, $columns, $catalogDependency, $controllerName
		
		# Data load
		# Loads departments info to show it in the sidebar menu
		$data['departments'] = $this->departments_model->get_departments();
		
        $data['dataArray'] = $data['departments'];
		$data['columnsToUse'] = ['NAME'=>"Nombre",'TEXT_BODY'=>"Descripción"];
		$data['catalogDependency'] = null;
		$data['controllerName'] = 'departments';
		
        #/ Data load
        
        # Page information
        $data['title'] = "Lista de Empleado";
		$data['subtitle'] = "Empleado";
		$data['navnodes'] = ["Panel de Control", "Empleados","Lista de Empleados"];
		//Extra CSS needed
		$data['css'] = [
		];
		//Extra JS needed
		$data['eJS'] = [
            "public/datatables.net/js/jquery.dataTables.min.js",
            "public/datatables.net-bs/js/dataTables.bootstrap.min.js"
		];
		$this->load->view('templates/header',$data);
		$this->load->view('templates/navbar',$data);
		$this->load->view('templates/list',$data);
		$this->load->view('templates/scripts',$data);
		$this->load->view('templates/footer');
    }

	public function orgchart($dept){
		# Data load
		$this->load->model('jobs_model');
		$this->load->model('orgchart_model');
		# Loads departments info to show it in the sidebar menu
		$data['departments'] = $this->departments_model->get_departments();
		$data['jobs'] = $this->jobs_model->get_jobs();
		# Get department personnel
		$data['personnel'] = $this->orgchart_model->get_personnel($dept);
		#/ Data load
		# Page information
		$data['title'] = "Organigrama";
		$data['subtitle'] = "Nombre del Departamento";
		$data['navnodes'] = ["Nombre del departamento","Información General"];
		//Extra CSS needed
		$data['css'] = [
			"getorgchart/getorgchart.css",
			"css/orgchart.css"
		];
		//Extra JS needed
		$data['eJS'] = [
			"public/getorgchart/getorgchart.js"
		];


		$this->load->view('templates/header',$data);
		$this->load->view('templates/navbar',$data);
		$this->load->view('departments/orgchart', $data);
		$this->load->view('templates/scripts');
		$this->load->view('templates/footer');
	}
	
	public function info($args){
		$id = $args[0];
		# Data load
		# Loads departments info to show it in the sidebar menu
		$data['info'] = $this->departments_model->get_department_info($id);
		# /Data load
		# Page information
		$data['title'] = "Información General";
		$data['subtitle'] = "Nombre del Departamento";
		$data['navnodes'] = ["Nombre del departamento","Información General"];
		# /Page information
		$this->load->view('templates/header',$data);
		$data['departments'] = $this->departments_model->get_departments();
		$this->load->view('templates/navbar',$data);
		$this->load->view('departments/info');
		$this->load->view('templates/scripts');
		$this->load->view('templates/footer');
	}

	public function articles($dept){
		# Data load
		$data['departments'] = $this->departments_model->get_departments();
		# /Data load
		# Page information
		$data['title'] = "Temas";
		$data['subtitle'] = "Nombre del Departamento";
		$data['navnodes'] = ["Nombre del departamento","Temas"];
		# /Page information
		$this->load->view('templates/header',$data);
		$data['args'] = $dept;
		$this->load->view('templates/navbar',$data);
		$this->load->view('departments/articles');
		$this->load->view('templates/scripts');
		$this->load->view('templates/footer');
	}

	public function newarticle($dept){
		
		# Data load
		$data['departments'] = $this->departments_model->get_departments();
		# /Data load
		# Page information
		$data['title'] = "Temas";
		$data['subtitle'] = "Nombre del Departamento";
		$data['navnodes'] = ["Nombre del departamento","Temas"];
		# /Page information
		$this->load->view('templates/header',$data);
		$data['args'] = $dept;
		$this->load->view('templates/navbar',$data);
		$this->load->view('departments/newarticle');
		$this->load->view('templates/scripts');
		$this->load->view('templates/footer');
	}

	// Main screen, here every option available will be shown
	public function links(){
		$this->load->view('templates/header');
		$this->load->view('templates/navbar_only');
		
		$this->load->view('templates/footer_no_sidebar');
	}

}
?>