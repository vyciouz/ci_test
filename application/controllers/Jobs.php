<?php
class Jobs extends CI_Controller{


	public function view(){
		$this->load->view('templates/header');
		$data['departments'] = $this->departments_model->get_departments();
		
		$this->load->view('templates/navbar',$data);
		$this->load->view('templates/scripts');
		$this->load->view('templates/footer');	
	}

	public function new(){

		$this->load->helper('form');
		# Page information
		$data['title'] = "Nuevo Usuario";
		$data['subtitle'] = "Usuarios";
		$data['navnodes'] = ['Panel de Control',"Usuarios","Nuevo Usuario"];
		# /Page information
		$data['css'] = array("bower_components/select2/dist/css/select2.min.css");
		$this->load->view('templates/header',$data);
		$data['departments'] = $this->departments_model->get_departments();
		$this->load->view('templates/navbar_only',$data);
		$this->load->view('users/new');
		$this->load->view('templates/scripts');
		$this->load->view('templates/footer');
	}

	public function list(){
		#list.php necesita 4 argumentos
		# $dataArray, $columns, $catalogDependency, $controllerName
		
		# Data load
		$data['departments'] = $this->departments_model->get_departments();
		$this->load->model('jobs_model');
		# Loads departments info to show it in the sidebar menu
		$data['dataArray'] = $this->jobs_model->get_jobs();
		$data['columnsToUse'] = ['NAME'=>"Puesto",'ACTIVE'=>"Activo"];
		$data['catalogDependency'] = null;
		$data['controllerName'] = 'jobs';
		
        #/ Data load
        
        # Page information
        $data['title'] = "Lista de Puestos";
		$data['subtitle'] = "Puestos";
		$data['navnodes'] = ["Panel de Control", "Empleados","Lista de Empleados"];
		//Extra CSS needed
		$data['css'] = [
		];
		//Extra JS needed
		$data['eJS'] = [
            "public/datatables.net/js/jquery.dataTables.min.js",
            "public/datatables.net-bs/js/dataTables.bootstrap.min.js"
		];
		$this->load->view('templates/header',$data);
		$this->load->view('templates/navbar',$data);
		$this->load->view('templates/list',$data);
		$this->load->view('templates/scripts',$data);
		$this->load->view('templates/footer');
    }
}
?>
