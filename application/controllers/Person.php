<?php
class Person extends CI_Controller{
	public function view($personId){
		# Data load
		# Loads departments info to show it in the sidebar menu
		$this->load->model('jobs_model');
		$data['departments'] = $this->departments_model->get_departments();
		$data['person'] = $this->personnel_model->get_person_info($personId);
		$data['personnel'] = $this->personnel_model->get_personnel();
		$data['jobs'] = $this->jobs_model->get_jobs();
		
		$data['mode'] = 'profile';

		#/ Data load
		# Page information
		$data['title'] = "Perfil de Empleado";
		$data['subtitle'] = "Empleado";
		$data['navnodes'] = ["Nombre del departamento","Editar Persona"];
		//Extra CSS needed
		$data['css'] = ["css/person.css"];
		//Extra JS needed
		// $data['js'] = [""];
		$this->load->view('templates/header',$data);
		$this->load->view('templates/navbar',$data);
        $this->load->view('templates/person',$data);
		$this->load->view('templates/scripts');
		$this->load->view('templates/footer');
	}

	public function delete($id){
		$data['table'] = "t_personnel";
		$data['id'] = $id;
		$data['redirect'] = "index.php/person/list";
		$this->load->view('templates/delete',$data);
	}

	public function list(){
		#list.php necesita 4 argumentos
		# $dataArray, $columns, $catalogDependency, $controllerName
		
		# Data load
		$this->load->model('users_model');
		$this->load->model('jobs_model');
		
        $data['dataArray'] = $this->personnel_model->get_personnel();
		$data['columnsToUse'] = [
			'SURENAME_1'=>"Apellido Paterno",
			'SURENAME_2'=>"Apellido Materno",
			'NAMES'=>"Nombre",
			'ID_DEPT'=>"Departamento"
		];
		$data['catalogDependency'] = array(
			"ID_DEPT"=>array(
				"TABLE"=>"cat_departments","searchInCOL"=>"ID","resultInCOL"=>"NAME"
			));
		$data['controllerName'] = 'person';
		# Loads departments info to show it in the sidebar menu
		$data['departments'] = $this->departments_model->get_departments();
        #/ Data load
        
        # Page information
        $data['title'] = "Lista de Empleado";
		$data['subtitle'] = "Empleado";
		$data['navnodes'] = ["Panel de Control", "Empleados","Lista de Empleados"];
		//Extra CSS needed
		$data['css'] = [
		];
		//Extra JS needed
		$data['eJS'] = [
            "public/datatables.net/js/jquery.dataTables.min.js",
            "public/datatables.net-bs/js/dataTables.bootstrap.min.js"
		];
		$this->load->view('templates/header',$data);
		$this->load->view('templates/navbar',$data);
		$this->load->view('templates/list',$data);
		$this->load->view('templates/scripts',$data);
		$this->load->view('templates/footer');
    }
    
    public function edit($personId){
		# Data load
		# Loads departments info to show it in the sidebar menu
		$this->load->model('jobs_model');
		$data['departments'] = $this->departments_model->get_departments();
		$data['person'] = $this->personnel_model->get_person_info($personId);
		$data['personnel'] = $this->personnel_model->get_personnel();
		$data['jobs'] = $this->jobs_model->get_jobs();
		
		$data['mode'] = 'edit';

		#/ Data load
		# Page information
		$data['title'] = "Editar Información";
		$data['subtitle'] = "Empleado";
		$data['navnodes'] = ["Nombre del departamento","Editar Persona"];
		//Extra CSS needed
		$data['css'] = ["css/person.css"];
		//Extra JS needed
		// $data['js'] = [""];
		$this->load->view('templates/header',$data);
		$this->load->view('templates/navbar',$data);
        $this->load->view('templates/person',$data);
        
		$this->load->view('templates/scripts');
		$this->load->view('templates/footer');
    }
	
    public function new(){
		# Data load
		# Loads departments info to show it in the sidebar menu
		$this->load->model('jobs_model');
		$data['departments'] = $this->departments_model->get_departments();
		// $data['person'] = $this->personnel_model->get_person_info();
		$data['personnel'] = $this->personnel_model->get_personnel();
		$data['jobs'] = $this->jobs_model->get_jobs();
		
		$data['mode'] = 'new';

		#/ Data load
		# Page information
		$data['title'] = "Nuevo Empleado";
		$data['subtitle'] = "Empleado";
		$data['navnodes'] = ["Nombre del departamento","Editar Persona"];
		//Extra CSS needed
		$data['css'] = ["css/person.css"];
		//Extra JS needed
		// $data['js'] = [""];
		$this->load->view('templates/header',$data);
		$this->load->view('templates/navbar',$data);
        $this->load->view('templates/person',$data);
        
		$this->load->view('templates/scripts');
		$this->load->view('templates/footer');
	}
}
?>