<div class="box">
    <div class="box-header">
        <h3 class="box-title">Hover Data Table</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
            <div class="row"><div class="col-sm-6"></div>
            <div class="col-sm-6"></div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                    <thead>
                        <tr role="row">
                        <?php
                        /* Columnas de la tabla a dibujar. */
                        /*
                        list.php ocupa llamarse desde el controlador con 4 argumentos para funcionar correctamente.
                        Ocupa $dataArray como el arreglo de información traído de la base de datos, $dataColumns como arreglo de las columnas a usar,
                        */
                        DrawTableHeaders($columnsToUse);
                        ?>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        /* Generación del cuerpo de la tabla*/
                        DrawTable($dataArray, $columnsToUse,$catalogDependency,$controllerName);?> 
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-md-12">
                         <?php
                        
                        $url = base_url()."index.php/" . $controllerName . "/new";?>
                        <a href="<?php echo $url?>"><button class="btn btn-primary">Nuevo</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.box-body -->
</div>
<script>
  $(function () {
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

<?php
/********************************************************************************************/
/* ************************************** FUNCTIONS  ************************************** */
/********************************************************************************************/

function DrawTableHeaders($array){
    $count = 0;
    foreach ($array as $key=>$value): 
        if ($count == 0) {?>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="<?php echo $value?>">
                <?php echo $value?>
            </th>
            <?php
        }else{?>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="<?php echo $value?>">
                <?php echo $value?>
            </th>
            <?php
        }?>
    <?php endforeach ?>
            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Controles">Controles</th>
    <?php
}

function DrawTable($data, $COLUMNS ,$arrayColNameToTable, $controller){
    //Cada registro encontrado
    foreach ($data as $key => $value): ?>
    <tr role="row" class="odd"><?php
        //Cada columna a buscar
        foreach ($COLUMNS as $colKEY => $colVALUE) {
            //Si la columna a buscar existe dentro del arreglo arrayColNameToTable como arrayColNameToTable[$colNAME], 
            if (isset($arrayColNameToTable[$colKEY]) && !empty($arrayColNameToTable[$colKEY])) {
                $ci =& get_instance();
                $searchValue = $value[$colKEY];
                $TABLE = $arrayColNameToTable[$colKEY]['TABLE'];
                $COL = $arrayColNameToTable[$colKEY]['searchInCOL'];
                $resCOL = $arrayColNameToTable[$colKEY]['resultInCOL'];
                $query = $ci->db->query("SELECT * FROM $TABLE WHERE $COL = $searchValue;");
                $queryResult = $query->result_array();
                $result = $queryResult;
                ?><td><?php echo $result[0][$resCOL];?></td><?php                
            } else {?>
                <td><?php echo $value[$colKEY] ?></td><?php
            }
        }?>
        
        
        <td>
        <a href="<?php echo base_url() . 'index.php/' . $controller . '/view/' . $value['ID']?>" class="btn btn-xs">
        <i class="fa fa-search"></i> Ver
        </a>
        <a href="<?php echo base_url() . 'index.php/' . $controller . '/edit/' . $value['ID']?>" class="btn btn-xs">
        <i class="fa fa-edit"></i> Editar
        </a>
        <a href="<?php echo base_url() . 'index.php/' . $controller . '/delete/' . $value['ID']?>" class="btn btn-xs">
        <i class="fa fa-remove"></i> Borrar
        </a>
        </td>
        </tr><?php
    endforeach;
}
?>