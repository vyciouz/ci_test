<?php
/*
This file is used for 3 different pages by 3 different controllers.
New
Edit
Profile
*/
?>

<?php

/*************************************************************************************************/
/* MAIN BODY */ 

/*
Conditionated display
If full edit changes are saved, POST vars are send to query array to be saved into database
Then, page redirects itself without POST values and changes are shown.
*/
showData($dataArray,$columnsToUse,$catalogDependency);


/********************************************************************************************/
/* ************************************** FUNCTIONS  ************************************** */ 
/********************************************************************************************/

function getSelectOptions($array, $isPerson, $colId, $colName){
    $result = array();
    if($isPerson){
        foreach($array as $key=>$value){
            array_push(
                $result, 
                ["ID"=>$array[$key][$colId],
                "NAME"=>$array[$key]['SURENAME_1'] . " " . $array[$key]['SURENAME_2'] . " " . $array[$key]['NAMES']
            ]);
        }
    }else{
        foreach($array as $key=>$value){
            array_push($result, ["ID"=>$array[$key][$colId],"NAME"=>$array[$key][$colName]]);
        }
    }
    
    return $result;
}

function showData($data, $COLUMNS ,$arrayColNameToTable){?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Información</h3>
            </div>
                
            <!-- table -->
            <div class="table-responsive">
                <table class="table no-margin">
                    <tbody>
                    <?php
                    foreach ($data as $key => $value): ?>
                        <?php
                        foreach ($COLUMNS as $colKEY => $colVALUE) {?>
                        <tr>
                            <td style="width:20%;"><?php echo $colVALUE ?></td>
                            <td><?php echo $value[$colKEY] ?></td>
                        </tr>
                            <?php
                        };?>
                        
                    <?php                    
                    endforeach ?>
                    </tbody>
                </table>
            </div>
            <!-- /table -->
        </div>
        <!-- /Box Container -->
    </div>
</div>
<?php
}

function inputForm($type, $id, $name, $value, $array){
    switch($type){
        case 'text':
            ?>
            <input
                type="<?php echo $type?>"
                value="<?php echo $value?>"
                name="<?php echo $name?>"
                id="<?php echo $name?>"
            >
            <i id="check-<?php echo $name?>" class="fa fa-fw fa-check" style="display: none"></i>
            <i id="remove-<?php echo $name?>"class="fa fa-fw fa-remove" style="display: none"></i>
            <?php
        break;
        case 'select':
            ?>
            <select
                id="<?php echo $name?>"
                name="<?php echo $name?>">
                <option value="0"> ---- </option>
                
            <?php foreach($array as $key=>$value): ?>
                <option value="<?php echo $value['ID'] ?>">
                    <?php echo $value['NAME'] ?>
                </option>
            <?php endforeach ?>
            </select>
            <?php
        break;
    }
}
?>