<?php
/*
This file is used for 3 different pages by 3 different controllers.
New
Edit
Profile
*/
?>

<?php
//Get array as ID=>NAME of data results. Boolean is to define if data obtained is form a person or a thing/concept.
$OptionJobs = getSelectOptions($jobs, false, 'ID','NAME');
$OptionDepatrments = getSelectOptions($departments, false, 'ID','NAME');
$OptionsBosses = getSelectOptions($personnel, true, 'ID','SURENAME_1');

/*
* Edit or view mode.
* In case some information is missing (boss, job, department, this takes care of it.
*/
if($mode!='new'){
    $bossId = $person[0]['ID_BOSS'];
    $jobId = $person[0]['ID_JOB'];
    $deptId = $person[0]['ID_DEPT'];

    if(empty($deptId)){
        $deptId = 0;
        $deptName = "N/A";
    }else{
        $dept = $this->departments_model->get_department_info($deptId);
        $deptName = $dept[0]['NAME'];
    }
    if(empty($bossId)){
        $bossName = "N/A";
        $bossId = 0;
    }else{
        $boss = $this->personnel_model->get_person_info($bossId);
        $bossName = $boss[0]['NAMES'] . " " . $boss[0]['SURENAME_1'] . " " . $boss[0]['SURENAME_2'];
    }
    if(empty($jobId)){
        $jobId = 0;
        $jobName = "";
    }else{
        $job = $this->jobs_model->get_job_info($jobId);
        $jobName = $job[0]['NAME'];
    }

    //Input (or select) fields with formatted with data needed
    //nameOfField => [Description, typeOfInput, singleValue, arrayOfValuesForSelects]
    //arrayOfValuesForSelects is only needed when the field needs to be and Option select
    $datosPersonales = [
        "name"=>["Nombre(s)", "type"=>"text", "value"=>$person[0]['NAMES'], "array"=>null],
        "aPaterno"=>["Apellido paterno", "type"=>"text", "value"=>$person[0]['SURENAME_1'], "array"=>null],
        "aMaterno"=>["Apellido materno", "type"=>"text", "value"=>$person[0]['SURENAME_2'], "array"=>null]
    ];
    $datosLaborales = [
        "dept"=>["Departamento", "type"=>"select", "value"=>$deptName, "array"=>$OptionDepatrments],
        "job"=>["Puesto", "type"=>"select", "value"=>$jobName, "array"=>$OptionJobs],
        "boss"=>["Jefe", "type"=>"select", "value"=>$bossName, "array"=>$OptionsBosses],
        "email"=>["Correo", "type"=>"text", "value"=>$person[0]['EMAIL'], "array"=>null]
    ];
}else{
    /* if new */
    $datosPersonales = [
        "name"=>["Nombre(s)", "type"=>"text", "value"=>"", "array"=>null],
        "aPaterno"=>["Apellido paterno", "type"=>"text", "value"=>"", "array"=>null],
        "aMaterno"=>["Apellido materno", "type"=>"text", "value"=>"", "array"=>null]
    ];
    $datosLaborales = [
        "id"=>      ["No. de empleado", "type"=>"text",     "value"=>"", "array"=>null],
        "dept"=>    ["Departamento",    "type"=>"select",   "value"=>"", "array"=>$OptionDepatrments],
        "job"=>     ["Puesto",          "type"=>"select",   "value"=>"", "array"=>$OptionJobs],
        "boss"=>    ["Jefe",            "type"=>"select",   "value"=>"", "array"=>$OptionsBosses],
        "email"=>   ["Correo",          "type"=>"text",     "value"=>"", "array"=>null]
    ];
}




/*************************************************************************************************/
/* MAIN BODY */ 

/*
Conditionated display
If full edit changes are saved, POST vars are send to query array to be saved into database
Then, page redirects itself without POST values and changes are shown.
*/
/* BIG IF */
if(isset($_POST) && !empty($_POST)): ?>
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> ¡Perfil actualizado!</h4>
    La información ha sido actualizada exitosamente.
</div>
<?php

$dataArray = array(
    'NAMES' => $_POST['name'],
    'SURENAME_1' => $_POST['aPaterno'],
    'SURENAME_2' => $_POST['aMaterno'],
    'ID_DEPT' => $_POST['dept'],
    'ID_JOB' => $_POST['job'],
    'ID_BOSS' => $_POST['boss'],
    'EMAIL' => $_POST['email']
);
print_r($mode);
switch($mode){
    case "edit":
        header( "refresh:3;url=" . $person[0]['ID'] );
        $this->db->where('id', $person[0]['ID']);
        $this->db->update('t_personnel', $dataArray);
        break;
    case "new":
        $dataArray['ID']=$_POST['id'];
        $this->db->insert('t_personnel', $dataArray);
        break;

}
?>

<?php

/* It's easier this way. */
/* BIG ELSE */
else: ?>
<div class="row">
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-body box-profile">
                <img class="profile-user-img img-responsive img-circle" style="height:200px; width: 200px; " src="<?php echo base_url()?>public/img/anonymous-user.png" alt="User profile picture">
                <h3 class="profile-username text-center"><?php echo (isset($person)?$person[0]['NAMES']:"Nuevo empleado")?></h3>
                <p class="text-muted text-center"><?php echo (isset($jobName)?$jobName:"")?></p>
                <p class="text-muted text-center"><?php echo (isset($person)? "No. de emplado: " . $person[0]['ID']:"")?></p>
            </div>
            <!-- /.box-body -->
          </div>
    </div>
</div>
<?php
// 
if($mode == 'edit' || $mode == 'new'):?>
<form action="" method="post">    
<?php endif;
    personData($datosPersonales, "Datos Personales", $mode);
    personData($datosLaborales, "Datos Laborales", $mode);
    // print_r($personnel);
    ?>
    <?php if($mode == 'edit' || $mode == 'new'):?>
    <div class="row">
        <div class="col-md-12 buttonHolder">
        <button id="save_all" style: "margin: 0">
            Guargar todos los cambios
        </button>
        </div>
    </div>
</form>
    <?php endif ?>
<?php
endif /* BIG ENDIF */ ?> 


<script type="text/javascript">
<?php if($mode == 'edit'):?>
$(".get-prev-page").click(function(){
    console.log('hello');
});

$(document).ready(function(){
    $("#dept option[value=<?php echo $deptId?>]").attr("selected", "selected");
    $("#boss option[value=<?php echo $bossId?>]").attr("selected", "selected");
    $("#job option[value=<?php echo $jobId?>]").attr("selected", "selected");
});
<?php else: ?>
<?php
/* Este ajax checa si el ID ingresado para el nuevo empleado se encuentra disponible */
?>
$(document).ready(function(){
    var id;
    var target_url;
    $( "#id" ).change(function() {
        id = $("#id" ).val();
        target_url = "<?php echo base_url();?>index.php/validator/newEmployee/" + id;
        if(id != "" && id !=0){
            $.ajax({
            url: target_url,
            method: "POST",
            data: id,
            dataType: 'json',
            async: true,
            beforeSend: function(){
                // Handle the beforeSend event
            },
            success: function(response){
                // Handle the complete event
                if(response.length != 0){
                    $("#check-id").css("display", "none");
                    $("#remove-id").css("display", "inline-block");

                }else{
                    $("#check-id").css("display", "inline-block");
                    $("#remove-id").css("display", "none");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
                }
            })//ajax
        }else{
            //hacer los checks invisibles
            $("#check-id").css("display", "none");
            $("#remove-id").css("display", "none");
            //inhabilitar el botón de guardar cambios
        }
        

    })
})
<?php endif ?>
</script>

<?php
/********************************************************************************************/
/* ************************************** FUNCTIONS  ************************************** */ 
/********************************************************************************************/

function getSelectOptions($array, $isPerson, $colId, $colName){
    $result = array();
    if($isPerson){
        foreach($array as $key=>$value){
            array_push(
                $result, 
                ["ID"=>$array[$key][$colId],
                "NAME"=>$array[$key]['SURENAME_1'] . " " . $array[$key]['SURENAME_2'] . " " . $array[$key]['NAMES']
            ]);
        }
    }else{
        foreach($array as $key=>$value){
            array_push($result, ["ID"=>$array[$key][$colId],"NAME"=>$array[$key][$colName]]);
        }
    }
    
    return $result;
}

function personData($array, $title, $mode){?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $title ?></h3>
            </div>
                
            <!-- table -->
            <div class="table-responsive">
                <table class="table no-margin">
                    <tbody>
                    <?php
                    foreach($array as $key=>$value):
                        switch($mode):
                            case 'edit':?>
                                <tr class="edit-field">
                                    <td style="width:20%;"><?php echo $value[0] ?>: </td>
                                    <td>
                                        <?php inputForm($value["type"],$key,$key,$value["value"],$value["array"]); ?>
                                    </td>
                                </tr>
                            <?php 
                            break;
                            case 'new':?>
                                <tr class="edit-field">
                                    <td style="width:20%;"><?php echo $value[0] ?>: </td>
                                    <td>
                                        <?php inputForm($value["type"],$key,$key,"",$value["array"]); ?>
                                    </td>
                                </tr>
                            <?php 
                            break;
                            case 'profile':?>
                                <tr>
                                    <td style="width:20%;"><?php echo $value[0] ?>: </td>
                                    <td style="width:60%;"><?php echo $value["value"] ?></td>
                                </tr>
                            <?php
                        endswitch;
                    endforeach ?>
                    </tbody>
                </table>
            </div>
            <!-- /table -->
        </div>
        <!-- /Box Container -->
    </div>
</div>
<?php
}

function inputForm($type, $id, $name, $value, $array){
    switch($type){
        case 'text':
            ?>
            <input
                type="<?php echo $type?>"
                value="<?php echo $value?>"
                name="<?php echo $name?>"
                id="<?php echo $name?>"
            >
            <i id="check-<?php echo $name?>" class="fa fa-fw fa-check" style="display: none"></i>
            <i id="remove-<?php echo $name?>"class="fa fa-fw fa-remove" style="display: none"></i>
            <?php
        break;
        case 'select':
            ?>
            <select
                id="<?php echo $name?>"
                name="<?php echo $name?>">
                <option value="0"> ---- </option>
                
            <?php foreach($array as $key=>$value): ?>
                <option value="<?php echo $value['ID'] ?>">
                    <?php echo $value['NAME'] ?>
                </option>
            <?php endforeach ?>
            </select>
            <?php
        break;
    }
}
?>