<?php
if (!empty($_POST)) {
    // print_r($_POST);
    $result = true;
    foreach ($columnsToUse as $key => $value) {
        // echo "$key<br>";
        if ($_POST[$key] == "") {
            // echo "$key =>". $_POST[$key] ."<br>";
            $result = false;
        }else{
            // echo "$key =>". $_POST[$key] ."<br>";
            
        }
    }
    // print_r($result);
    if ($result) {
        $this->db->insert($table, $_POST);
    }
}else{
    
}
?>
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo $title?></h3>
        <div class="box-tools pull-right">
            <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button> -->        
        </div>
    </div>
    <!-- /.box-header -->
    <form method = "POST" class="form-horizontal">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
              
                    <div class="form-group">
 
                    <?php
                        DrawEditFields($columnsToUse);
                    ?>
                    </div>
               
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    
    <div class="box-footer">
        <?php echo form_submit(array('id' => 'submit', 'value' => 'Registrar', 'class' => 'btn btn-primary')); ?>
        <?php echo form_close(); ?>
    </div>
    </form>
</div>

<?php
function DrawEditFields($columnsToUse){
    
    foreach ($columnsToUse as $key => $value) {
        switch ($value['type']) {
            case 'text':
                DrawTextInput($columnsToUse[$key], $key);
                break;

            case 'select':
                DrawSelectInput($columnsToUse[$key], $key);
                break;

            case 'password':
                DrawPasswordInput($columnsToUse[$key], $key);
                break;
            
            default:
                # code...
                break;
        }
    }
    
}

function DrawTextInput($array, $colName){
    $attributes = array(
        'class' => 'col-sm-2 control-label'
    );?>
    <?php echo form_label($array['name'],$colName,$attributes); ?>
    <div class="col-sm-10 form-group">
        <?php echo form_input(array('id' => $colName, 'name' => $colName,'class'=>'form-control')); ?>
    </div>
<?php
}

function DrawSelectInput($array, $colName){
    $attributes = array(
        'class' => 'col-sm-2 control-label'
    );?>
    <label for=""><?php echo $array['name']?></label>
    <select  class ="form-control" name="<?php echo $colName?>" id="<?php echo $colName?>">
        <option value="0">N/A</option>
    <?php
    if ($array['dependency']!=null) {
        $ci =& get_instance();
        $values = $ci->db->get($array['dependency']['table']);
        foreach ($values->result_array() as $row) {?>
            <option value="<?php echo $row[$array['dependency']['inCol']] ?>"><?php echo $row[$array['dependency']['outCol']] ?></option>
        <?php
        }
    }
    ?>
    
    </select>
    <br>
<?php
}

function DrawPasswordInput($array, $colName){
    $attributes = array(
        'class' => 'col-sm-2 control-label'
    );?>
    <?php echo form_label($array['name'],$colName,$attributes); ?>
    <div class="col-sm-10 form-group">
        <input class = "form-control" type="password" name="<?php echo $colName?>" id="<?php echo $colName?>">
    </div>
    <?php echo form_label("Verificar " . $array['name'],$colName,$attributes); ?>
    <div class="col-sm-10 form-group">
        <input class = "form-control" type="password" id="<?php echo $colName?>_verify">
        <div id="divCheckPasswordMatch"></div>
    </div>
    <script>
        function checkPasswordMatch() {
            var password = $("#<?php echo $colName?>").val();
            var confirmPassword = $("#<?php echo $colName?>_verify").val();
            if (password != confirmPassword)
                $("#divCheckPasswordMatch").html("Contraseña no coincide!");
            else
                $("#divCheckPasswordMatch").html("");
        }

        $(document).ready(function () {
            $("#<?php echo $colName?>").change(checkPasswordMatch);
            $("#<?php echo $colName?>_verify").change(checkPasswordMatch);
        });
    </script>
<?php
}

?>