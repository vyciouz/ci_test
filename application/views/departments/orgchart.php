<?php
/**
* 
*/

class Personnel
{
	public $id;
	public $idBoss;
	public $names;
	public $desc;
	public $email;
}
/*************************************************************************************************/
/* MAIN BODY */ 
if(isset($personnel) && !empty($personnel)):
    printOrgChart($personnel);
    // print_r($jobs);
else:
    echo "Notihng here";
endif;

/*************************************************************************************************/
/* FUNCTIONS */ 
function printOrgChart($personnel){
    ?>
    <div id="people"></div>

    <script type="text/javascript">
        var peopleElement = document.getElementById("people");
        var orgChart = new getOrgChart(peopleElement, {
            theme: "ula",
            color: "green",
            scale: 0.6,
            levelSeparation: 200,
            
            primaryFields: ["Nombres", "ApellidoPaterno", "ApellidoMaterno", "title", "phone", "mail"],
            photoFields: ["image"],
            gridView: true,
            layout: getOrgChart.MIXED_HIERARCHY_RIGHT_LINKS,
            siblingSeparation: 100,
            subtreeSeparation: 100,
            dataSource: [
                <?php foreach ($personnel as $key => $value): ?>
                {
                    id: <?php echo $value['ID']?>,
                    parentId: <?php echo $value['ID_BOSS']?>,
                    Nombres: <?php echo "\"" . $value['NAMES'] . "\""?>,
                    ApellidoPaterno: <?php echo "\"" . $value['SURENAME_1'] . "\""?>,
                    ApellidoMaterno: <?php echo "\"" . $value['SURENAME_2'] . "\""?>,
                    <?php  ?>
                    title: <?php echo "\"" . $value['ID_JOB'] . "\""?>,
                    phone: "678-772-470",
                    mail: <?php echo "\"" . $value['EMAIL'] . "\""?>,
                    adress: "Atlanta, GA 30303",
                    image: "<?php echo base_url()?>public/img/anonymous-user.png"
                }
                <?php if (isset($personnel[$key+1])): ?>
                ,
                <?php endif?>
                <?php endforeach ?>
            ]
        });
    </script>

    <script type="text/javascript">
    $(".get-prev-page").click(function(){
        console.log('hello');
    });

    $(document).on('click',".get-btn",function(){
        var id = $(this).attr('data-btn-id');
        var action = $(this).attr('data-btn-action');
        console.log("ID: " + id);
        console.log("ID" + $(this).attr('data-btn-id'));
        console.log(action);
        switch(action){
            case 'edit':
                window.location.href = "<?php echo base_url()?>index.php/person/edit/" + id;
                break;
            case 'details':
                window.location.href = "<?php echo base_url()?>index.php/person/view/" + id;
                break;
            case 'add':
                window.location.href = "<?php echo base_url()?>index.php/person/new/" + id;
                break;
            case 'del':
                alert("WARNING, This does nothing yet but will maybe delete this user.");
                break;
            
        }
    });
    </script>
    <?php
}
?>