<body class="hold-transition login-page" style="background-image: url(<?php echo base_url() ?>public/img/bg2.jpg); background-size: 100% 100%;">
<div class="login-box">
    <div class="login-logo">
        <a style="color: black;" href="<?php echo base_url()?>"><span class="logo-lg">
            <img src="<?php echo base_url();?>/public/img/wiki_logo.png" alt="" style="height: 30px">Wiki NL</span></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body" style="background-color:rgba(4, 0, 0, 0.6); color: white; border: 1px solid white;">
        <p class="login-box-msg">Bienvenido</p>
        <form action="<?php echo base_url() ?>" method="post">
            <div class="form-group has-feedback">
                <input type="email" class="form-control" placeholder="Usuario" style="background-color:rgba(4, 0, 0, 0); color: white; border: 1px solid white;">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Contraseña" style="background-color:rgba(4, 0, 0, 0); color: white; border: 1px solid white;" >
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat" style="background-color: #393; border: 1px solid white;">Entrar</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
        <a href="#" style="color: #6ad; font-weight: bold;">Olvidé mi contraseña</a><br>
        <a href="register.html" style="color: #6ad; font-weight: bold;" class="text-center">Registrarse</a>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- iCheck -->
<script src="<?php echo base_url() ?>public/iCheck/icheck.min.js"></script>
<script>
    $(function () {
        $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
        });
    });
</script>
</body>