<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Wiki Gobierno NL</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/AdminLTE/bower_components/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/AdminLTE/bower_components/Ionicons/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/AdminLTE/dist/css/AdminLTE.min.css">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
	   folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/AdminLTE/dist/css/skins/_all-skins.min.css">
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	<!-- Google Font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>public/css/style.css">
	<!-- jQuery 3 -->
	<script src="<?php echo base_url(); ?>public/AdminLTE/bower_components/jquery/dist/jquery.min.js"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="<?php echo base_url(); ?>public/AdminLTE/bower_components/jquery-ui/jquery-ui.min.js"></script>
	<link rel="stylesheet" href="<?php echo base_url()?>public/css/style.css">
	<style>
	.misc-data{
		margin:30px;
		color: white;
		padding-top: 50px;
		padding-bottom: 50px;
		min-width: 350px;
		margin: auto;
		font-size: 40px;
		text-align:center;
	}
	.img-footer{
		width: 100%;
		max-width: 85px;
	}
	</style>
</head>

<body>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				Iniciar sesión
			</div><br>
		</div>
	</div>
	<!-- Top -->
	<!-- 
		| | |
	 -->
	<div class="row">
		<div class="col-md-3">
		</div>
		<div class="col-md-6" style="text-align:center">
			<div class="row">
				<div class="col-md-12" style="margin-top: 50px;">
					<h1>GOBIERNO NL</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12" style="padding: 20px;">
					<img src="<?php echo base_url()?>public/img/escudo.png"  style="width:200px" alt="">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<input type="text" name="q" class="form-control" placeholder="Búsqueda..." style="max-width:400px; display:inline-block">
					<button type="submit" name="search" id="search-btn" class="btn btn-primary btn-flat"><i class="fa fa-search"></i>
                	</button>
				</div>
			</div>
		</div>
		<div class="col-md-3">
		</div>
	</div>
	<!-- Mid -->
	<!-- 
		| | |
	 -->
	<div class = "row">
		<div class="col-md-12" style="background-color:#333; margin-top: 50px;">
			<div class="col-md-4">
				<div class = "col-md-12 misc-data" style="text-align:center;">
				<a href="<?php echo base_url();?>index.php/person/list">
					<div class="col-md-12">
						<i class="fa fa-fw fa-users"></i>
					</div>
					<div class="col-md-12">
						<?php echo $this->db->count_all('t_personnel');?> trabajadores registrados
					</div>
				</a>
				</div>
			</div>
			<div class="col-md-4">
				<div class = "col-md-12 misc-data">
				<a href="<?php echo base_url();?>index.php/departments/list">
					<div class="col-md-12">
						<i class="fa fa-fw fa-building"></i>
					</div>
					<div class="col-md-12">
						<?php echo $this->db->count_all('cat_departments');?> departamentos registrados
					</div>
				</a>
				</div>
			</div>
			<div class="col-md-4">
				<div class = "col-md-12 misc-data">
				<a href="<?php echo base_url();?>index.php/users/list">
					<div class="col-md-12">
						<i class="fa fa-fw fa-building"></i>
					</div>
					<div class="col-md-12">
						<?php echo $this->db->count_all('t_users');?> usuarios registrados
					</div>
				</a>
				</div>
			</div>
		</div>
	</div>
	<!-- Panel negro con información estadística -->
	<!-- "Footer" -->
	<div class = "row" style="padding-top: 40px; text-align: center">
	<?php
	 /*
	 * Esto se manejará a través de la base de datos, como una tabla con secciones y enlaces relevantes.
	 */
	// Enlaces importantes
	$lSide = [
		"a"=>["img"=>"public/img/book_icon.png", "text"=>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris quis magna mauris.", "url"=>"departments", "title"=>""],
		"c"=>["img"=>"public/img/book_icon.png", "text"=>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris quis magna mauris.", "url"=>"departments", "title"=>""],
		"d"=>["img"=>"public/img/book_icon.png", "text"=>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris quis magna mauris.", "url"=>"departments", "title"=>""],
		"e"=>["img"=>"public/img/book_icon.png", "text"=>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris quis magna mauris.", "url"=>"departments", "title"=>""]
	];
	// Enlaces dentro del dominio
	$rSide = [
		"a"=>["img"=>"public/img/book_icon.png", "text"=>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris quis magna mauris.", "url"=>"departments", "title"=>"Departamentos"],
		"b"=>["img"=>"public/img/book_icon.png", "text"=>"Texto.", "url"=>"departments", "title"=>""],
		"c"=>["img"=>"public/img/book_icon.png", "text"=>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris quis magna mauris.", "url"=>"departments", "title"=>""],
		"f"=>["img"=>"public/img/book_icon.png", "text"=>"Texto.", "url"=>"departments", "title"=>""],
		"ewf"=>["img"=>"public/img/book_icon.png", "text"=>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris quis magna mauris.", "url"=>"departments", "title"=>""],
		"wef"=>["img"=>"public/img/book_icon.png", "text"=>"Texto.", "url"=>"departments", "title"=>""],
		"w3rf"=>["img"=>"public/img/book_icon.png", "text"=>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris quis magna mauris.", "url"=>"departments", "title"=>""],
		"dawk"=>["img"=>"public/img/book_icon.png", "text"=>"Texto.", "url"=>"departments", "title"=>""],
		"d"=>["img"=>"public/img/book_icon.png", "text"=>"Texto.", "url"=>"departments", "title"=>""],
	];
	?>
		<div class="col-md-3">
			<?php foreach($lSide as $key => $value):?>
				<div class="col-md-12" style="padding-top: 40px;padding-bottom: 40px;">
					<div class="col-md-3">
						<img src="<?php echo base_url() . $value['img']?>" alt="" class="img-footer">
					</div>
					<div class="col-md-9" style="text-align-center">
						<p><?php echo $value["title"]?></p>
						<p style="max-width:300px; margin: auto;"><?php echo $value['text'] ?></p>
						<a href="" class="btn btn-xs">
							<i class="fa fa-edit"></i> Editar
						</a>
						<a class="btn btn-xs">
							<i class="fa fa-remove"></i> Borrar
						</a>
					</div>
				</div>
			<?php endforeach?>
			<div class="col-md-12" style="padding-top: 40px;padding-bottom: 40px;">

				<a class="btn btn-app" href="<?php echo base_url()?>index.php/links/new/1">
					<i class="fa fa-plus"></i> Nuevo
				</a>
			</div>
		</div>
		<div class="col-md-9">
			<?php footerLinks($rSide);?>
		</div>
	</div><!-- row -->
</div>

<script
  src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
  integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
  crossorigin="anonymous"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>

<?php
function footerLinks($array){?>
	<?php foreach($array as $key => $value):?>
		<div class="col-md-3" style="padding-top: 40px;padding-bottom: 40px; height: 150px; ">
			<div class="col-md-3">
				<img src="<?php echo base_url() . $value['img']?>" alt="" class="img-footer">
			</div>
			<div class="col-md-9" style="text-align-center">
				<a href="">
					<b><p><?php echo $value["title"]?></p></b>
					<p style="max-width:300px; margin: auto;"><?php echo $value['text'] ?></p>
				</a>
                <a href="" class="btn btn-xs">
                    <i class="fa fa-edit"></i> Editar
                </a>
                <a class="btn btn-xs">
                    <i class="fa fa-remove"></i> Borrar
                </a>
			</div>
		</div>
	<?php endforeach?>
	<div class="col-md-3" style="padding-top: 40px;padding-bottom: 40px; border: 1px solid #999; border-radius: 20px; min-height: 150px;">
			<div class="col-md-3">
				<a class="btn btn-xs">
                    <i class="fa fa-plus"></i> Nuevo
                </a>
			</div>
			<div class="col-md-9" style="text-align-center">
				<a href="">
					<b><p>Nuevo</p></b>
					<p style="max-width:300px; margin: auto;">Nuevo</p>
				</a>
			</div>
		</div>
	<?php
}?>