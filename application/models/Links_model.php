<?php
class Links_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	// methods always inside the class
	// remember to add model to autoload if needed
	public function get_links(){
	    $query = $this->db->query("SELECT * FROM t_links;");
	    $result = $query->result_array();
	    return $result;
	}

	public function get_link($id){
		$query = $this->db->query("SELECT * FROM t_links WHERE ID = $id;");
	    $result = $query->result_array();
	    return $result;
    }
    
    public function get_rside_links(){
        $query = $this->db->query("SELECT * FROM t_links WHERE AND TYPE = 1;");
	    $result = $query->result_array();
	    return $result;
	}
	public function get_lside_links(){
        $query = $this->db->query("SELECT * FROM t_links WHERE AND TYPE = 2;");
	    $result = $query->result_array();
	    return $result;
    }
}
?>