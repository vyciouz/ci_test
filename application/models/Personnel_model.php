<?php
class Personnel_model extends CI_Model{
    public function __construct(){
		$this->load->database();
    }
    // methods always inside the class
    // remember to add model to autoload
    public function get_person_info($id){
        $query = $this->db->query("SELECT * FROM t_personnel WHERE ID = $id");
        $result = $query->result_array();
        return $result;
    }

    public function get_personnel(){
        $this->db->order_by("SURENAME_1", "ASC");
        $query = $this->db->get('t_personnel');
	    $result = $query->result_array();
	    return $result;
    }

}
?>