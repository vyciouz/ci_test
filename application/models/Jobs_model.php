<?php
class Jobs_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	// methods always inside the class
	// remember to add model to autoload if needed
	public function get_jobs(){
	    $query = $this->db->query("SELECT * FROM cat_jobs ORDER BY NAME ASC;");
	    $result = $query->result_array();
	    return $result;
	}

	public function get_job_info($id){
		$query = $this->db->query("SELECT * FROM cat_jobs WHERE ID = $id;");
	    $result = $query->result_array();
	    return $result;
	}
}
?>