<?php
class Departments_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	// methods always inside the class
	// remember to add model to autoload
	public function get_departments(){
		$this->db->order_by("NAME", "ASC");
		$this->db->where_not_in("ID",0);
		$query = $this->db->get('cat_departments');
	    $result = $query->result_array();
	    return $result;
	}

	public function get_department_info($id){
		$query = $this->db->query("SELECT * FROM cat_departments WHERE ID = $id");
		$result = $query->result_array();
		// $array = array("ID" => $id);
		// $result = $this->db->where("ID", $id);
	    return $result;
	}

}
?>