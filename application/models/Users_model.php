<?php
class Users_model extends CI_Model{
    public function __construct(){
		$this->load->database();
    }
    // methods always inside the class
    // remember to add model to autoload
    public function get_user($id){
        $query = $this->db->query("SELECT * FROM t_users WHERE ID = $id");
        $result = $query->result_array();
        return $result;
    }

    public function get_users(){
        $this->db->order_by("USERNAME", "ASC");
        $query = $this->db->get('t_users');
	    $result = $query->result_array();
	    return $result;
    }
}
?>