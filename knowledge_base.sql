/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : knowledge_base

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-11-02 09:36:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `cat_departments`
-- ----------------------------
DROP TABLE IF EXISTS `cat_departments`;
CREATE TABLE `cat_departments` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(30) DEFAULT NULL,
  `TEXT_BODY` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cat_departments
-- ----------------------------
INSERT INTO `cat_departments` VALUES ('0', 'Sin departamento', 'No');
INSERT INTO `cat_departments` VALUES ('1', 'TEST_DEPT', 'TEST DE DESCRIPCION');
INSERT INTO `cat_departments` VALUES ('4310', 'Dirección de Tenologías', 'L');
INSERT INTO `cat_departments` VALUES ('4330', 'Dirección de Informática', 'L');
INSERT INTO `cat_departments` VALUES ('4340', 'Subsecretaría de Tenologías', 'L');

-- ----------------------------
-- Table structure for `cat_jobs`
-- ----------------------------
DROP TABLE IF EXISTS `cat_jobs`;
CREATE TABLE `cat_jobs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `ACTIVE` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cat_jobs
-- ----------------------------
INSERT INTO `cat_jobs` VALUES ('1', 'CAPTURISTA', '1');
INSERT INTO `cat_jobs` VALUES ('2', 'AUXILIAR ADMINISTRATIVO', '1');
INSERT INTO `cat_jobs` VALUES ('3', 'ENCARGADO DE SISTEMA', '1');
INSERT INTO `cat_jobs` VALUES ('4', 'JEFE DE SISTEMAS', '1');
INSERT INTO `cat_jobs` VALUES ('5', 'ANALISTA DE SOPORTE TECNICO', '1');
INSERT INTO `cat_jobs` VALUES ('6', 'JEFE PROGRAMADOR INTEGRADOR DE INGRESO', '1');
INSERT INTO `cat_jobs` VALUES ('7', 'JEFE DE CAPTURA', '1');
INSERT INTO `cat_jobs` VALUES ('8', 'JEFE DE INFRAESTRUCTURA', '1');
INSERT INTO `cat_jobs` VALUES ('9', 'JEFE DE DESARROLLO DE SISTEMAS', '1');
INSERT INTO `cat_jobs` VALUES ('10', 'ANALISTA SENIOR', '1');
INSERT INTO `cat_jobs` VALUES ('11', 'COORDINADORA DE SISTEMAS', '1');
INSERT INTO `cat_jobs` VALUES ('12', 'OPERADOR SOPORTE TECNICO', '1');
INSERT INTO `cat_jobs` VALUES ('13', 'COORDINADOR DE SOPORTE TECNICO', '1');
INSERT INTO `cat_jobs` VALUES ('14', 'INGENIERO SENIOR', '1');
INSERT INTO `cat_jobs` VALUES ('15', 'INGENIERO DE SERVICIO', '1');
INSERT INTO `cat_jobs` VALUES ('16', 'JEFE DE OPERACION Y CONTROL', '1');
INSERT INTO `cat_jobs` VALUES ('17', 'ANALISTA', '1');
INSERT INTO `cat_jobs` VALUES ('18', 'JEFE DE OPERACIONES', '1');
INSERT INTO `cat_jobs` VALUES ('19', 'SECRETARIA DE DIRECCION', '1');
INSERT INTO `cat_jobs` VALUES ('20', 'ASISTENTE DE INFORMACION', '1');
INSERT INTO `cat_jobs` VALUES ('21', 'ANALISTA DE SISTEMAS', '1');
INSERT INTO `cat_jobs` VALUES ('22', 'COORDINADOR INFORMATICA', '1');
INSERT INTO `cat_jobs` VALUES ('23', 'ASISTENTE DE DIRECTOR', '1');
INSERT INTO `cat_jobs` VALUES ('24', 'ANALISTA PROGRAMADOR', '1');
INSERT INTO `cat_jobs` VALUES ('25', 'JEFE DE SISTEMAS TESORERIA INGRESOS', '1');
INSERT INTO `cat_jobs` VALUES ('26', 'ANALISTA DE SISTEMAS Y BASE DE DATOS', '1');
INSERT INTO `cat_jobs` VALUES ('27', 'COORDINADOR DE ESTRATEGIA DE IMAGEN GUBE', '1');
INSERT INTO `cat_jobs` VALUES ('28', 'JEFE DE ANALISIS DE INFORMACION', '1');
INSERT INTO `cat_jobs` VALUES ('29', 'ASISTENTE', '1');
INSERT INTO `cat_jobs` VALUES ('30', 'JEFE DE REDES', '1');
INSERT INTO `cat_jobs` VALUES ('31', 'SOPORTE TECNICO', '1');
INSERT INTO `cat_jobs` VALUES ('32', 'JEFE DE RED DE DATOS', '1');
INSERT INTO `cat_jobs` VALUES ('33', 'JEFE DE ASESORIA Y SOPORTE', '1');
INSERT INTO `cat_jobs` VALUES ('34', 'COORDINADOR DEL SITIO CENTRAL', '1');
INSERT INTO `cat_jobs` VALUES ('35', 'JEFE DE SOPORTE TECNICO', '1');
INSERT INTO `cat_jobs` VALUES ('36', 'JEFE DE LABORATORIO DE SOPORTE TECNICO', '1');
INSERT INTO `cat_jobs` VALUES ('37', 'OPERADOR DE SITE LOCAL', '1');
INSERT INTO `cat_jobs` VALUES ('38', 'JEFE DE SOPORTE TECNICO (INFORMATICA)', '1');
INSERT INTO `cat_jobs` VALUES ('39', 'COORDINADOR DE INFORMATICA', '1');
INSERT INTO `cat_jobs` VALUES ('40', 'COORDINADORA ADMINISTRATIVA', '1');
INSERT INTO `cat_jobs` VALUES ('41', 'OPERADOR ANALISTA', '1');
INSERT INTO `cat_jobs` VALUES ('42', 'COORDINADOR DE PROYECTOS E INTERNET', '1');
INSERT INTO `cat_jobs` VALUES ('43', 'ANALISTA/DESARROLLADOR SR', '1');
INSERT INTO `cat_jobs` VALUES ('44', 'JEFE DE SISTEMAS DE DESARROLLO', '1');
INSERT INTO `cat_jobs` VALUES ('45', 'ANALISTA DE INFORMACION', '1');
INSERT INTO `cat_jobs` VALUES ('46', 'ANALISTA DE PROCESOS', '1');
INSERT INTO `cat_jobs` VALUES ('47', 'JEFE DE TECNOLOGIAS DE INFORMACION', '1');
INSERT INTO `cat_jobs` VALUES ('48', 'ANALISTA DE INFORMATICA Y PRESUPUESTO', '1');
INSERT INTO `cat_jobs` VALUES ('49', 'JEFE DE INFRAESTRUCTURA, SERV. DE COMPUT', '1');
INSERT INTO `cat_jobs` VALUES ('50', 'COORDINADOR DE REDES Y TELECOMUNICACIO', '1');
INSERT INTO `cat_jobs` VALUES ('51', 'ANALISTA DE INFORMATICA', '1');
INSERT INTO `cat_jobs` VALUES ('52', 'EDITOR DE CONTENIDO PARA INTERNET', '1');
INSERT INTO `cat_jobs` VALUES ('53', 'AUXILIAR', '1');
INSERT INTO `cat_jobs` VALUES ('54', 'COORDINADORA DE DESARROLLO DE SISTEMAS P', '1');
INSERT INTO `cat_jobs` VALUES ('55', 'ARQUITECTO DE PROCESOS', '1');
INSERT INTO `cat_jobs` VALUES ('56', 'OPERADOR DE COMUNIDADES', '1');
INSERT INTO `cat_jobs` VALUES ('57', 'JEFE DE MERCADOTECNIA', '1');
INSERT INTO `cat_jobs` VALUES ('58', 'JEFE DE TELECOMUNICACIONES', '1');
INSERT INTO `cat_jobs` VALUES ('59', 'EDITOR', '1');
INSERT INTO `cat_jobs` VALUES ('60', 'JEFE PROGRAMADOR', '1');
INSERT INTO `cat_jobs` VALUES ('61', 'JEFE DE ESTRATEGIA Y PLANEACION', '1');
INSERT INTO `cat_jobs` VALUES ('62', 'SUBSECRETARIO DE TECNOLOGIAS', '1');
INSERT INTO `cat_jobs` VALUES ('63', 'ANALISTA DE ATENCION CIUDADANA', '1');
INSERT INTO `cat_jobs` VALUES ('64', 'COORDINADOR DE MERCADOTECNIA Y PLANEAC', '1');
INSERT INTO `cat_jobs` VALUES ('65', 'JEFE DE SISTEMAS PARA LA INTERACION CON', '1');
INSERT INTO `cat_jobs` VALUES ('66', 'COORDINADOR DE INFORMATICA EGRESOS', '1');
INSERT INTO `cat_jobs` VALUES ('67', 'COORDINADOR DE INFRAESTRUCTURA TECNOLO', '1');
INSERT INTO `cat_jobs` VALUES ('68', 'DIRECTORA DE TECNOLOGIAS', '1');
INSERT INTO `cat_jobs` VALUES ('69', 'DESARROLLO Y PROGRAMACION', '1');
INSERT INTO `cat_jobs` VALUES ('70', 'COORDINADORA DE INFORMATICA', '1');
INSERT INTO `cat_jobs` VALUES ('71', 'PROGRAMADOR Y DESARROLLADOR REDES', '1');
INSERT INTO `cat_jobs` VALUES ('72', 'COORDINADOR DE VOZ Y DATOS', '1');
INSERT INTO `cat_jobs` VALUES ('73', 'PROGRAMADOR JR.', '1');
INSERT INTO `cat_jobs` VALUES ('74', 'DESARROLLADOR JR.', '1');
INSERT INTO `cat_jobs` VALUES ('75', 'JEFE DE ADMINISTRACION DE PROYECTOS', '1');
INSERT INTO `cat_jobs` VALUES ('76', 'COORDINADOR DE PLANEACION Y ESTRATEGIA', '1');
INSERT INTO `cat_jobs` VALUES ('77', 'PROGRAMADOR JR', '1');

-- ----------------------------
-- Table structure for `cat_link_type`
-- ----------------------------
DROP TABLE IF EXISTS `cat_link_type`;
CREATE TABLE `cat_link_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cat_link_type
-- ----------------------------
INSERT INTO `cat_link_type` VALUES ('1', 'Enlace perteneciente al inicio de página');
INSERT INTO `cat_link_type` VALUES ('2', 'Enlace perteneciente a un departamento');

-- ----------------------------
-- Table structure for `cat_roles`
-- ----------------------------
DROP TABLE IF EXISTS `cat_roles`;
CREATE TABLE `cat_roles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cat_roles
-- ----------------------------
INSERT INTO `cat_roles` VALUES ('1', 'ADMIN');
INSERT INTO `cat_roles` VALUES ('2', 'Visitante');

-- ----------------------------
-- Table structure for `t_links`
-- ----------------------------
DROP TABLE IF EXISTS `t_links`;
CREATE TABLE `t_links` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(255) DEFAULT NULL,
  `BODY` varchar(255) DEFAULT NULL,
  `TYPE` int(11) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `ID_DEPT` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_links
-- ----------------------------

-- ----------------------------
-- Table structure for `t_personnel`
-- ----------------------------
DROP TABLE IF EXISTS `t_personnel`;
CREATE TABLE `t_personnel` (
  `ID` int(11) NOT NULL,
  `ID_DEPT` int(11) DEFAULT NULL,
  `ID_BOSS` int(11) DEFAULT NULL,
  `ID_JOB` varchar(255) DEFAULT NULL,
  `NAMES` varchar(255) DEFAULT NULL,
  `SURENAME_1` varchar(255) DEFAULT NULL,
  `SURENAME_2` varchar(255) DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `PHOTO` varchar(255) DEFAULT NULL,
  `PHONE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_personnel
-- ----------------------------
INSERT INTO `t_personnel` VALUES ('1', '1', '0', '1', 'TESTO', null, null, '1', '1', null);
INSERT INTO `t_personnel` VALUES ('2', '1', '1', '1', 'TESTIN', null, null, '1', '1', null);
INSERT INTO `t_personnel` VALUES ('75118', '4340', '448804', null, 'ELVA PATRICIA GARCIA GONZALEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('108252', '4310', '448804', null, 'MARIA IRMA PACHUCA ZUL', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('161275', '4310', '846526', null, 'SERGIO MENDOZA HERNANDEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('186481', '4310', '492168', null, 'FRANCISCO RANGEL GUTIERREZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('186490', '4310', '492168', null, 'JOSE HUMBERTO GONZALEZ MERCADO', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('206146', '4310', '514947', null, 'RUBEN GUADALUPE RODRIGUEZ LOPEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('206762', '4310', '514947', null, 'GERARDO JAVIER ESPINOSA DUEÑEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('217358', '4310', '492168', null, 'JOSE NATIVIDAD DE LA PEÑA OYERVIDES', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('218035', '4310', '448804', null, 'NANCY DE LOURDES VILLARREAL RAMOS', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('227945', '4310', '448804', null, 'LETICIA MARIA GARZA MAYORGA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('240564', '4310', '448804', null, 'RAMIRO SAENZ TREVIÑO', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('248075', '4310', '846526', null, 'RIGOBERTO PACHECO QUINTANILLA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('261363', '4310', '522125', null, 'JOSE GENARO HERNANDEZ MARFILEÑO', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('261390', '4310', '522125', null, 'VICTOR MANUEL LICEA RODRIGUEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('282528', '4310', '618620', null, 'JULIA CARRILLO RODRIGUEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('282653', '4310', '827583', null, 'REYNOL TREVIÑO MARTINEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('294999', '4310', '522125', null, 'LEONCIO HERRERA CALVILLO', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('296193', '4310', '514947', null, 'REGINALDO AREVALO MONTEMAYOR', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('302685', '4310', '593871', null, 'JUAN CARLOS FLORES QUINTANA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('303255', '4310', '522125', null, 'MARIA RAMONA CALDERON CARREON', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('303353', '4310', '522125', null, 'FRANCISCO JAVIER HERNANDEZ SUSTAITA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('313897', '4310', '522125', null, 'MIGUEL ESPINOSA CARDENAS', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('315813', '4310', '586236', null, 'ELIDA LOPEZ RAMIREZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('318892', '4310', '448804', null, 'JUAN CARLOS MEDINA QUINTERO', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('320076', '4310', '806819', null, 'DELIA GUADALUPE SANCHEZ RUIZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('323475', '4310', '467507', null, 'HECTOR RODRIGUEZ MENDEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('335873', '4310', '492168', null, 'GUADALUPE FRANCISCO PARRA GARCIA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('358938', '4310', '492168', null, 'GUADALUPE MARGARITA FLORES CORPUS', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('360041', '4310', '282653', null, 'MARCELINO RAMOS VAZQUEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('374849', '4310', '522125', null, 'MELISA SALINAS RIOS', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('418275', '4310', '618620', null, 'EDUARDO FIGUEROA REYNA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('418551', '4310', '492168', null, 'JUAN PEDRO LARA OLIVA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('419229', '4330', '829116', null, 'AZENETH ESCOBEDO SALDIVAR', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('436460', '4310', '618620', null, 'SILVIA GUADALUPE TOSCANO BENITEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('436497', '4310', '618620', null, 'REBECA DIAZ ESPINOSA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('442293', '4310', '618620', null, 'CRISTINA VILLARREAL RIVAS', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('448804', '4310', '829116', null, 'SANDRA MARCELA DEL RIO GARCIA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('454897', '4310', '618620', null, 'NYDIA MARIA SALAZAR MOLINA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('459927', '4310', '806819', null, 'VICTOR ARTURO SEVILLA GARBOZA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('467507', '4310', '0', null, 'LILIANA GUADALUPE LOPEZ LOERA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('467801', '4310', '467507', null, 'DENISSE GARCIA RODRIGUEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('469041', '4310', '448804', null, 'DANTE RANGEL REYNA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('471985', '4310', '282653', null, 'RAUL GONZALEZ AYALA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('473483', '4310', '618620', null, 'JOSE ARTEMIO MARTINEZ PUENTE', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('479147', '4310', '514947', null, 'ABEL SOLIS CORPUS', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('480617', '4310', '593871', null, 'MARIO ALBERTO VELAZQUEZ CASTILLO', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('486586', '4310', '846526', null, 'FRANCISCO JAVIER AGUILAR CANTU', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('492168', '4310', '829116', null, 'ALMA EDITH REYNOSO NAJERA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('501639', '4310', '514947', null, 'PEDRO CORTES VILLEGAS', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('511735', '4310', '514947', null, 'JOSE LUIS MARTINEZ COVARRUBIAS', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('514947', '4310', '827583', null, 'MIGUEL ANGEL RIVERA ROSALES', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('522125', '4330', '819681', null, 'MARIA DEL CARMEN GUAJARDO REYES', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('524383', '4310', '618620', null, 'FERNANDO RIVERA CHAVARRIA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('524472', '4330', '827583', null, 'JOSE DE JESUS BENAVIDES TOVAR', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('524589', '4310', '593871', null, 'JUAN JOSE NORIEGA PADILLA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('524927', '4310', '618620', null, 'HILDA FRANCISCA GUEL GARCIA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('532392', '4310', '857158', null, 'JOSEFINA VILLANUEVA RUEDA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('536307', '4310', '448804', null, 'HECTOR EDUARDO AGUIRRE VELA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('546770', '4310', '524472', null, 'RICARDO LOPEZ JACQUES', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('552807', '4310', '448804', null, 'NORA NELY ARANGO RODRIGUEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('552816', '4310', '514947', null, 'DOMINGO SILVESTRE PEREZ LOPEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('552825', '4310', '448804', null, 'JOSE GUADALUPE CASTILLO GARCIA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('552834', '4310', '448804', null, 'MARIA LUISA MENDOZA MEDINA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('552843', '4310', '448804', null, 'MARIA DEL CARMEN TORRES DELGADO', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('565562', '4310', '492168', null, 'ADALBERTO HERNANDEZ RODRIGUEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('565571', '4310', '448804', null, 'ROSALINDA LOPEZ RAMONES', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('565580', '4310', '448804', null, 'IRMA PATRICIA ALMAGUER GARCIA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('565606', '4310', '448804', null, 'SERGIO ARTEAGA GUERRERO', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('571653', '4310', '846526', null, 'SAMUEL EDGAR BAUZA RAMIREZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('572072', '4310', '618620', null, 'NIDIA ROCIO REYES URBINA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('576505', '4310', '492168', null, 'MIGUEL ANGEL RODRIGUEZ SANCHEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('580161', '4310', '492168', null, 'LETICIA LOERA MARTINEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('584907', '4310', '593871', '0', 'Jorge Alejandro', 'Ostiguin', 'Martinez', '', null, null);
INSERT INTO `t_personnel` VALUES ('585746', '4310', '618620', null, 'JOSE ALBERTO ALVAREZ GARCIA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('586183', '4310', '282653', null, 'JUAN ALBERTO HERNANDEZ SANTIAGO', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('586236', '4310', '829116', null, 'ANDREA MARISOL GONZALEZ GONZALEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('590552', '4310', '846526', null, 'REMIGIO GONZALEZ ALANIS', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('593871', '4310', '827583', '0', 'Arturo Vladimir', 'Castillo', 'Rodriguez', '', null, null);
INSERT INTO `t_personnel` VALUES ('594405', '4310', '586236', null, 'FRANCISCO ESEQUIEL CERDA DURAN', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('600095', '4330', '524472', '0', 'Humberto', 'López', 'González', '', null, null);
INSERT INTO `t_personnel` VALUES ('601692', '4310', '618620', null, 'LETICIA RAMIREZ MORALES', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('605545', '4310', '0', null, 'LILIANA LETICIA DANIEL QUINTANILLA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('613269', '4310', '586236', null, 'CLAUDIA ROMERO CELIS', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('618620', '4310', '829116', null, 'VALERIA NASHIELLY TREVIÑO AVALOS', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('618979', '4310', '467507', null, 'CARLOS LICURGO VAZQUEZ ARIZPE', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('622259', '4310', '846526', null, 'JESUS LAZARO MEZA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('639634', '4310', '514947', null, 'CARLOS ANTONIO GONZALEZ NIETO', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('648786', '4310', '820124', null, 'HECTOR JAVIER MAYORGA RIOS', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('654822', '4310', '820124', null, 'ALVARO MARCELO RENDON DE LA GARZA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('667998', '4310', '819681', null, 'BERENICE MONTSERRAT MENDO CABRERA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('687412', '4310', '846526', null, 'MIGUEL ANGEL HERRERA ABREGO', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('704385', '4310', '820124', null, 'CHRYSTIAME JANNETTE RAMIREZ ALEMAN', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('731891', '4310', '618620', null, 'OZIEL ALBERTO CRUZ GARZA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('806819', '4340', '819681', null, 'JORGE JULIAN GONZALEZ GARCIA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('819681', '4340', '0', null, 'JAVIER ENRIQUE GOMEZ VAZQUEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('820106', '4310', '586236', null, 'FRANCISCO JAVIER MENDOZA SANTOYO', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('820124', '4310', '829116', null, 'MARIO ALBERTO TORRES GONZALEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('820277', '4310', '820124', null, 'ROBERTO EROS CABALLERO PEÑA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('823069', '4310', '586236', null, 'ELIAS VALERA DORADO', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('827583', '4310', '819681', null, 'MANUEL ERNESTO GALVAN RIVERA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('827592', '4310', '857158', null, 'JORGE ALBERTO MATEOS MENCHACA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('829090', '4310', '846526', null, 'LUIS FRANCISCO VINAJA CHAVEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('829116', '4310', '819681', null, 'MITZY ANAHI MACIAS GARZA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('829590', '4310', '806819', null, 'ROBERTO CARLOS MONTOYA MONSIVAIS', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('829992', '4310', '823069', null, 'RICARDO GARZA VERASTEGUI', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('830211', '4310', '0', '0', 'Eva Karina', 'Estudillo', 'Ruiz', '', null, null);
INSERT INTO `t_personnel` VALUES ('830293', '4340', '0', null, 'ABRIL AIDE GONZALEZ GONZALEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('839230', '4310', '823069', null, 'RODOLFO MENCHACA RODRIGUEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('846526', '4310', '819681', null, 'DIDIER DUILIO MERLO RODRIGUEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('846562', '4340', '829116', null, 'MANUEL DEL COS LARA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('847276', '4310', '820124', null, 'EMMA MARLENE JIMENEZ BARAJAS', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('847285', '4310', '586236', null, 'MONICA RIVERA MAZCORRO', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('847294', '4310', '618620', null, 'ASAF ELIEL SAENZ TREVIZO', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('847301', '4310', '586236', null, 'OSCAR ABRAHAM GONZALEZ FLORENTINO', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('854339', '4340', '448804', null, 'ANDRES DE JESUS TERRIQUEZ OJEDA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('857149', '4340', '857158', null, 'FABIOLA ELIZONDO LABORDE', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('857158', '4340', '819681', null, 'RODRIGO HERRERA AMAYA', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('858513', '4310', '823069', null, 'EFRAIN ROBLES GONZALEZ', null, null, null, null, null);
INSERT INTO `t_personnel` VALUES ('858520', '1', '1', '1', 'Testerino', 'Tulipán', 'Taelo', 'si', null, null);
INSERT INTO `t_personnel` VALUES ('858641', '1', '1', '1', 'Testerino', 'Tulipán', 'Taelo', 'si', null, null);
INSERT INTO `t_personnel` VALUES ('8564531', '1', '1', '1', 'Tilín Tilín', 'Tronamaba', 'Talurio', 'no', null, null);

-- ----------------------------
-- Table structure for `t_users`
-- ----------------------------
DROP TABLE IF EXISTS `t_users`;
CREATE TABLE `t_users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(255) DEFAULT NULL,
  `PWD` varchar(255) DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `ID_ROLE` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_users
-- ----------------------------
INSERT INTO `t_users` VALUES ('1', 'admin', 'admin', 'admin', null);
